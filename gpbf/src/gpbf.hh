#pragma once

#include <array>
#include <vector>
#include <cassert>

#include "gpbf_detail.inl"

//! Specializations of write_binary for vector and string.
namespace gpbf_customization
{
	template <typename Ostream, typename T, typename Alloc, typename>
	auto write_binary(Ostream & os, std::vector<T, Alloc> const & v) -> unsigned;
	template <typename Istream, typename T, typename Alloc, typename>
	auto read_binary(Istream & is, std::vector<T, Alloc> & v, unsigned byte_count) -> bool;

	template <typename Ostream, typename T, typename Traits, typename Alloc>
	auto write_binary(Ostream & os, std::basic_string<T, Traits, Alloc> const & v) -> unsigned;
	template <typename Istream, typename T, typename Traits, typename Alloc>
	auto read_binary(Istream & is, std::basic_string<T, Traits, Alloc> & v, unsigned byte_count) -> bool;
}

namespace gpbf
{
	//! Write an object to an output stream in binary. It will try to call t.write_binary(os) or write_binary(os, t).
	//! If not found, it will write a binary dump of the object. Returns the number of bytes written.
	constexpr auto write_binary = [](auto & os, auto const & t) -> decltype(gpbf_detail::write_binary_impl(os, t, gpbf_detail::priority_tag<3>()))
	{
		return gpbf_detail::write_binary_impl(os, t, gpbf_detail::priority_tag<3>());
	};
	//! Tries to read an object from byte_count bytes from the stream is. It will try to call t.read_binary(os, byte_count) 
	//! or read_binary(os, t, byte_count). Otherwise it will read into the memory layout of the object.
	//! Returns whether the object was successfully read.
	constexpr auto read_binary = [](auto & is, auto & t, unsigned byte_count) -> decltype(gpbf_detail::read_binary_impl(is, t, byte_count, gpbf_detail::priority_tag<3>()))
	{
		return gpbf_detail::read_binary_impl(is, t, byte_count, gpbf_detail::priority_tag<3>());
	};
	//! Returns the number of bytes an object would write to an output stream. Doesn't perform any IO.
	constexpr auto binary_size = [](auto const & t) -> unsigned
	{
		struct MockOstream { constexpr void write(char const *, std::streamsize) const noexcept {} } mock_ostream;
		return write_binary(mock_ostream, t);
	};

	//! A gpbf file starts with a table of metadata describing the lumps in the file. Each lump
	//! is identified by a null terminated string of up to 16 bytes. The table entry also holds
	//! the offset from the start of the file to the start of the lump and the size in bytes of the lump.
	struct TableEntry
	{
		std::array<char, 16> id;
		unsigned offset;
		unsigned size;
	};
	using Table = std::vector<TableEntry>;

	//! Lump constructor. Reading and writing functions work with lumps, with are pairs of object + string id.
	template <typename T>
	constexpr auto lump(T && t, char const id[]) noexcept -> gpbf_detail::Lump<decltype(std::forward<T>(t))>;

	//! Writes a gpbf file with the given lumps to the given stream. Returns the number of bytes written.
	template <typename Ostream, typename ... Ts>
	auto write_file(Ostream & os, gpbf_detail::Lump<Ts> ... lumps) -> unsigned;

	//! Attempts to read the given lumps from the given stream in gpbf. For each lump, returns whether it was successfully read or not.
	//! A lump may fail to read because it is not present in the file or because the read_binary function fails.
	template <typename Istream, typename ... Ts>
	auto read_file(Istream & is, gpbf_detail::Lump<Ts> ... lumps) -> std::array<bool, sizeof...(Ts)>;

	//! Reads the lump table from a stream in gpbf. This table can be used to query the lumps individually with try_read_lump.
	template <typename Istream>
	auto read_table(Istream & is) -> std::vector<TableEntry>;

	//! Tries to read a lump from a stream in gpbf from which the table has already been read.
	template <typename Istream, typename T>
	auto try_read_lump(Istream & is, gpbf_detail::Lump<T> lump, Table const & table) -> bool;

	//! Traits to check if a type is binary readable/writable and to check if it uses the default behavior or a custom one.
	template <typename T> struct is_binary_writable : std::bool_constant<gpbf_detail::write_binary_priority_tag<T> >= 0> {};
	template <typename T> constexpr bool is_binary_writable_v = is_binary_writable<T>::value;
	template <typename T> struct is_binary_readable : std::bool_constant<gpbf_detail::read_binary_priority_tag<T> >= 0> {};
	template <typename T> constexpr bool is_binary_readable_v = is_binary_readable<T>::value;

	template <typename T> struct uses_default_write_binary : std::bool_constant<gpbf_detail::write_binary_priority_tag<T> == 0> {};
	template <typename T> constexpr bool uses_default_write_binary_v = uses_default_write_binary<T>::value;
	template <typename T> struct uses_default_read_binary : std::bool_constant<gpbf_detail::read_binary_priority_tag<T> == 0> {};
	template <typename T> constexpr bool uses_default_read_binary_v = uses_default_read_binary<T>::value;

} // namespace gpbf

#include "gpbf.inl"
