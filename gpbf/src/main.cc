#include "gpbf.hh"
#include "buffer_stream.hh"
#include <sstream>
#include <iostream>
#include <fstream>
#include <random>
#include <chrono>
#include <list>

int main()
{
	using gpbf::lump;

	char buffer[2048];

	gpbf::BufferOstream os(buffer, 2048);
	
	gpbf::write_file(os, lump(5, "foo"), lump(3.141592f, "bar"), lump('c', "baz"), 
		lump(std::vector<int>{1, 2, 3, 4, 5}, "v"));

	int foo;
	float bar;
	char baz;
	bool quux;
	double zyx;

	gpbf::BufferIstream is(buffer, 2048);
	auto const read = gpbf::read_file(is
		, lump(foo, "foo")
		, lump(bar, "bar")
		, lump(baz, "baz")
		, lump(quux, "quux")
		, lump(zyx, "zyx")
	);

	assert(read[0] == true);
	assert(read[1] == true);
	assert(read[2] == true);
	assert(read[3] == false);
	assert(read[4] == false);

	static_assert(gpbf::is_binary_writable_v<int>);
	static_assert(gpbf::is_binary_writable_v<std::vector<int>>);
	static_assert(!gpbf::is_binary_writable_v<std::list<int>>);
	static_assert(gpbf::uses_default_write_binary_v<int>);
	static_assert(!gpbf::uses_default_write_binary_v<std::vector<int>>);

	static_assert(gpbf::is_binary_readable_v<int>);
	static_assert(gpbf::is_binary_readable_v<std::vector<int>>);
	static_assert(!gpbf::is_binary_readable_v<std::list<int>>);
	static_assert(gpbf::uses_default_read_binary_v<int>);
	static_assert(!gpbf::uses_default_read_binary_v<std::vector<int>>);
}
