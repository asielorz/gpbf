namespace gpbf_detail
{

	template <int Priority> struct priority_tag : priority_tag<Priority - 1> {};
	template <> struct priority_tag<-1> {};
	template <int Priority> constexpr auto priority_tag_v = priority_tag<Priority>();

	template <typename Ostream, typename T>
	auto write_binary_impl(Ostream & os, T const & t, priority_tag<0>) -> std::enable_if_t<std::is_trivially_copyable_v<T>, unsigned>
	{
		os.write(reinterpret_cast<char const *>(std::addressof(t)), sizeof(T));
		return sizeof(T);
	}
	template <typename Ostream, typename T>
	auto write_binary_impl(Ostream & os, T const & t, priority_tag<1>) -> decltype(write_binary(os, t))
	{
		return write_binary(os, t);
	}
	template <typename Ostream, typename T>
	auto write_binary_impl(Ostream & os, T const & t, priority_tag<2>) -> decltype(t.read_binary(os))
	{
		return t.write_binary(os);
	}
	template <typename Ostream, typename T>
	auto write_binary_impl(Ostream & os, T const & t, priority_tag<3>) -> decltype(gpbf_customization::write_binary(os, t))
	{
		return gpbf_customization::write_binary(os, t);
	}

	template <typename Istream, typename T>
	auto read_binary_impl(Istream & is, T & t, unsigned byte_count, priority_tag<0>) -> std::enable_if_t<std::is_trivially_copyable_v<T>, bool>
	{
		is.read(reinterpret_cast<char *>(std::addressof(t)), byte_count);
		return true;
	}
	template <typename Istream, typename T>
	auto read_binary_impl(Istream & is, T & t, unsigned byte_count, priority_tag<1>) -> decltype(read_binary(is, t, byte_count))
	{
		return read_binary(is, t, byte_count);
	}
	template <typename Istream, typename T>
	auto read_binary_impl(Istream & is, T & t, unsigned byte_count, priority_tag<2>) -> decltype(t.read_binary(is, byte_count))
	{
		return t.read_binary(is, byte_count);
	}
	template <typename Istream, typename T>
	auto read_binary_impl(Istream & is, T & t, unsigned byte_count, priority_tag<3>) -> decltype(gpbf_customization::read_binary(is, t, byte_count))
	{
		return gpbf_customization::read_binary(is, t, byte_count);
	}

	constexpr std::array<char, 4> correct_file_id = { 'g', 'p', 'b', 'f' };
	struct FileHeader
	{
		std::array<char, 4> file_id;
		int lump_count;
	};

	template <typename T>
	struct Lump
	{
		char const * id;
		T object;
	};

	//***************************************************************************************************************
	// Traits

	template <typename Ostream, typename T>
	auto write_binary_priority_tag_impl(Ostream & os, T const & t, priority_tag<-1>) -> std::integral_constant<int, -1>;
	template <typename Ostream, typename T>
	auto write_binary_priority_tag_impl(Ostream & os, T const & t, priority_tag<0>) -> std::enable_if_t<std::is_trivially_copyable_v<T>, std::integral_constant<int, 0>>;
	template <typename Ostream, typename T>
	auto write_binary_priority_tag_impl(Ostream & os, T const & t, priority_tag<1>) -> std::conditional_t<false, decltype(write_binary(os, t)), std::integral_constant<int, 1>>;
	template <typename Ostream, typename T>
	auto write_binary_priority_tag_impl(Ostream & os, T const & t, priority_tag<2>) -> std::conditional_t<false, decltype(t.read_binary(os)), std::integral_constant<int, 2>>;
	template <typename Ostream, typename T>
	auto write_binary_priority_tag_impl(Ostream & os, T const & t, priority_tag<3>) -> std::conditional_t<false, decltype(gpbf_customization::write_binary(os, t)), std::integral_constant<int, 3>>;

	template <typename T>
	constexpr int write_binary_priority_tag = decltype(write_binary_priority_tag_impl(std::declval<std::ostream &>(), std::declval<T const &>(), priority_tag_v<3>))::value;

	template <typename Istream, typename T>
	auto read_binary_priority_tag_impl(Istream & is, T & t, unsigned byte_count, priority_tag<-1>) -> std::integral_constant<int, -1>;
	template <typename Istream, typename T>
	auto read_binary_priority_tag_impl(Istream & is, T & t, unsigned byte_count, priority_tag<0>) -> std::enable_if_t<std::is_trivially_copyable_v<T>, std::integral_constant<int, 0>>;
	template <typename Istream, typename T>
	auto read_binary_priority_tag_impl(Istream & is, T & t, unsigned byte_count, priority_tag<1>) -> std::conditional_t<false, decltype(read_binary(is, t, byte_count)), std::integral_constant<int, 1>>;
	template <typename Istream, typename T>
	auto read_binary_priority_tag_impl(Istream & is, T & t, unsigned byte_count, priority_tag<2>) -> std::conditional_t<false, decltype(t.read_binary(is, byte_count)), std::integral_constant<int, 2>>;
	template <typename Istream, typename T>
	auto read_binary_priority_tag_impl(Istream & is, T & t, unsigned byte_count, priority_tag<3>) -> std::conditional_t<false, decltype(gpbf_customization::read_binary(is, t, byte_count)), std::integral_constant<int, 3>>;

	template <typename T>
	constexpr int read_binary_priority_tag = decltype(read_binary_priority_tag_impl(std::declval<std::istream &>(), std::declval<T &>(), 0u, priority_tag_v<3>))::value;

} // namespace gpbf_detail
