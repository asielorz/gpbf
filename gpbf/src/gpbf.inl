#include <array>
#include <vector>
#include <cassert>

namespace gpbf_customization
{
	template <typename Ostream, typename T, typename Alloc, typename = std::enable_if_t<gpbf::uses_default_write_binary_v<T>>>
	auto write_binary(Ostream & os, std::vector<T, Alloc> const & v) -> unsigned
	{
		os.write(reinterpret_cast<char const *>(v.data()), v.size() * sizeof(T));
		return static_cast<unsigned>(v.size() * sizeof(T));
	}
	template <typename Istream, typename T, typename Alloc, typename = std::enable_if_t<gpbf::uses_default_read_binary_v<T>>>
	auto read_binary(Istream & is, std::vector<T, Alloc> & v, unsigned byte_count) -> bool
	{
		assert(byte_count % sizeof(T) == 0);
		v.resize(byte_count / sizeof(T));
		is.read(reinterpret_cast<char *>(v.data()), byte_count);
		return true;
	}

	template <typename Ostream, typename T, typename Traits, typename Alloc>
	auto write_binary(Ostream & os, std::basic_string<T, Traits, Alloc> const & v) -> unsigned
	{
		os.write(reinterpret_cast<char const *>(v.data()), v.size() * sizeof(T));
		return static_cast<unsigned>(v.size() * sizeof(T));
	}
	template <typename Istream, typename T, typename Traits, typename Alloc>
	auto read_binary(Istream & is, std::basic_string<T, Traits, Alloc> & v, unsigned byte_count) -> bool
	{
		assert(byte_count % sizeof(T) == 0);
		v.resize(byte_count / sizeof(T));
		is.read(reinterpret_cast<char *>(v.data()), byte_count);
		return true;
	}
}

namespace gpbf
{

	template <typename T>
	constexpr auto lump(T && t, char const id[]) noexcept -> gpbf_detail::Lump<decltype(std::forward<T>(t))>
	{
		return {id, std::forward<T>(t)};
	}

	template <typename Ostream, typename ... Ts>
	auto write_file(Ostream & os, gpbf_detail::Lump<Ts> ... lumps) -> unsigned
	{
		using namespace gpbf_detail;
		constexpr int lump_count = sizeof...(lumps);

		// Write header
		FileHeader header = {correct_file_id, lump_count};
		write_binary(os, header);

		// Get lump ids and sizes
		std::array<char const *, lump_count> const lump_ids = {lumps.id...};
		std::array<unsigned, lump_count> const lump_sizes = {binary_size(lumps.object)...};

		// Write lump table
		std::vector<TableEntry> table(lump_count);
		unsigned offset_acc = sizeof(header) + sizeof(TableEntry) * lump_count;
		for (int i = 0; i < lump_count; ++i)
		{
			strcpy_s(table[i].id.data(), table[i].id.size(), lump_ids[i]);
			table[i].size = lump_sizes[i];
			table[i].offset = offset_acc;
			offset_acc += lump_sizes[i];
		}
		write_binary(os, table);

		// Write lump data
		(static_cast<void>(write_binary(os, lumps.object)), ...);

		// Return number of bytes written.
		return offset_acc;
	}

	template <typename Istream>
	auto read_table(Istream & is) -> Table
	{
		using namespace gpbf_detail;

		FileHeader header;
		read_binary(is, header, sizeof(header));
		assert(header.file_id == correct_file_id);

		std::vector<TableEntry> table;
		read_binary(is, table, header.lump_count * sizeof(TableEntry));

		return table;
	}

	template <typename Istream, typename T>
	auto try_read_lump(Istream & is, gpbf_detail::Lump<T> lump, Table const & table) -> bool
	{
		char const * const id = lump.id;
		auto const it = std::find_if(table.begin(), table.end(), [id](TableEntry const & entry) { return strcmp(id, entry.id.data()) == 0; });
		if (it != table.end())
		{
			is.seekg(it->offset);
			return read_binary(is, lump.object, it->size);
		}
		return false;
	}

	template <typename Istream, typename ... Ts>
	auto read_file(Istream & is, gpbf_detail::Lump<Ts> ... lumps) -> std::array<bool, sizeof...(Ts)>
	{
		Table const table = read_table(is);
		return {try_read_lump(is, lumps, table)...};
	}

} // namespace gpbf