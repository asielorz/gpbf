#pragma  once

#include <cstring>
#include <cassert>

namespace gpbf
{

	struct BufferOstream
	{
		BufferOstream(void * buffer_, std::streamsize size_) : buffer(buffer_), size(size_) {}

		void write(char const * data, std::streamsize count) noexcept
		{
			assert(index + count <= size);
			memcpy(static_cast<char *>(buffer) + index, data, count);
			index += count;
		}

	private:
		void * buffer;
		std::streamsize size;
		std::streamsize index = 0;
	};

	struct BufferIstream
	{
		BufferIstream(void const * buffer_, std::streamsize size_) : buffer(buffer_), size(size_) {}

		void read(char * data, std::streamsize count) noexcept
		{
			assert(index + count <= size);
			memcpy(data, static_cast<char const *>(buffer) + index, count);
			index += count;
		}
		void seekg(std::streamsize pos)
		{
			assert(pos <= size);
			index = pos;
		}

	private:
		void const * buffer;
		std::streamsize size;
		std::streamsize index = 0;
	};

} // namespace gpbf
